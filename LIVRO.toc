\select@language {brazilian}
\contentsline {part}{{\def \penalty -\@M {}A desmedida na medida}}{9}
\contentsline {chapter}{1937--39}{11}
\contentsline {section}{22 de setembro.}{11}
\contentsline {section}{23 de setembro.}{13}
\contentsline {section}{23 de setembro.}{14}
\contentsline {section}{26 de setembro.}{14}
\contentsline {section}{30 de setembro.}{16}
\contentsline {section}{30 de setembro.}{17}
\contentsline {section}{2 de outubro.}{17}
\contentsline {section}{4 de outubro.}{18}
\contentsline {section}{10 de outubro.}{19}
\contentsline {section}{15 de outubro.}{19}
\contentsline {section}{17 de outubro.}{20}
\contentsline {section}{18 de outubro.}{21}
\contentsline {section}{20 de outubro.}{22}
\contentsline {section}{21 de outubro.}{23}
\contentsline {section}{25 de outubro.}{24}
\contentsline {section}{5 de novembro.}{24}
\contentsline {section}{6 de novembro.}{24}
\contentsline {section}{7 de novembro.}{25}
\contentsline {section}{8 de novembro.}{25}
\contentsline {section}{13 de novembro.}{26}
\contentsline {section}{16 de novembro.}{27}
\contentsline {section}{17 de novembro.}{27}
\contentsline {section}{22 de novembro.}{28}
\contentsline {section}{Dezembro.}{28}
\contentsline {section}{Dezembro.}{32}
\contentsline {section}{Dezembro.}{33}
\contentsline {section}{Fevereiro de 38.}{35}
\contentsline {section}{Fevereiro de 38.}{36}
\contentsline {section}{Abril de 38.}{37}
\contentsline {section}{Abril.}{38}
\contentsline {section}{Abril de 38.}{38}
\contentsline {section}{Maio.}{38}
\contentsline {section}{Maio.}{40}
\contentsline {section}{Junho.}{41}
\contentsline {section}{Junho.}{45}
\contentsline {section}{Agosto.}{47}
\contentsline {section}{21 de agosto de 1938.}{49}
\contentsline {section}{Dezembro de 38.}{60}
\contentsline {section}{Domingo.}{61}
\contentsline {section}{Dezembro.}{68}
\contentsline {section}{1939.}{74}
\contentsline {section}{Fevereiro.}{74}
\contentsline {section}{Março.}{77}
\contentsline {section}{Abril de 39.}{78}
\contentsline {part}{Apêndices}{83}
\contentsline {chapter}{Posfácio, \emph {por Nilson Silva}}{85}
\contentsline {section}{O lugar dos cadernos na obra de um escritor}{85}
\contentsline {section}{Filosofia dos limites}{93}
\contentsline {section}{O absurdo e a revolta}{100}
\contentsline {section}{O homem estrangeiro}{105}
\contentsline {section}{Romance, mito da História}{111}
\contentsline {section}{O romancista filósofo}{115}
\contentsline {section}{Bibliografia}{120}
\contentsline {chapter}{Cronologia}{123}
\contentsline {chapter}{Obras do autor}{127}
\contentsline {section}{Obras póstumas}{128}
\contentsfinish 
